﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Campfire : MonoBehaviour
{
    public Color color;
    public Light fireLight;
    public ParticleSystem fire;
    public float minIntensity = 0.75f;
    public float maxIntensity = 1.25f;

    private void Start()
    {
        fireLight.DOBlendableColor(color, 0.50f).SetLoops(-1, LoopType.Yoyo);
        fireLight.DOIntensity(Random.Range(minIntensity, maxIntensity), 2.0f).SetLoops(-1, LoopType.Yoyo);
    }

    public void StopFire()
    {
        fire.Stop();
        fireLight.DOKill();
        fireLight.DOIntensity(0, 1.50f);
    }
}
