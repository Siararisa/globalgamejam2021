﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
public class CharacterBell : MonoBehaviour
{
    public Animator animator;
    public CorgiController controller;
    public GameObject bell;
    public GameObject bellCollider;

    private bool isAquired = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        RingBell();
    }

    public void RingBell()
    {
        if(controller.State.IsGrounded && Input.GetMouseButtonDown(0) && isAquired)
        {
            animator.SetTrigger("RingBell");
            controller.gameObject.GetComponent<CharacterHorizontalMovement>().enabled = false;
            AudioManager.Instance.PlayTrack("bell_ring");
            MoreMountains.CorgiEngine.InputManager.Instance.InputDetectionActive = false;
            StartCoroutine(EnableCollider());
        }
    }

    IEnumerator EnableCollider()
    {
        bellCollider.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        bellCollider.gameObject.SetActive(false);
        controller.gameObject.GetComponent<CharacterHorizontalMovement>().enabled = true;
    }
    public void AcquireBell()
    {
        isAquired = true;
        bell.gameObject.SetActive(true);
    }
}
