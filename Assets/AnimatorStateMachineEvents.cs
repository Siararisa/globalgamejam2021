﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class AnimatorStateMachineEvents : MonoBehaviour
{

    public void EnableMovement()
    {
        MoreMountains.CorgiEngine.InputManager.Instance.InputDetectionActive = true;
    }
}
