﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Panda;

public class Pet : MonoBehaviour
{

    public Transform parent;
    public Vector3 bound;
    public Transform[] escapePoints;
    public Transform currentEscapePoint;
    public float idleSpeed = 5.0f, rotationSpeed = 20f;
    public float escapeSpeed = 10.0f;
    public Animator animator;


    [Task]
    bool IsScared = false;

    [Task]
    bool IsSeparated = true;

    [Task]
    bool IsSurprised = false;

    [Task]
    bool IsWait = false;

    private Vector3 initialPosition, nextMovementPoint;
    public float distance;

    private void Start()
    {
        CalculateNextMovementPoint();
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
            IsScared = !IsScared;
    }

    public void Scare()
    {
        IsScared = true;
    }

    private void CalculateNextMovementPoint()
    {
        transform.SetParent(parent);
        initialPosition = Vector3.zero;
        float posX = Random.Range(initialPosition.x - bound.x, initialPosition.x + bound.x);
        float posY = Random.Range(initialPosition.y - bound.y, initialPosition.y + bound.y);
        float posZ = Random.Range(initialPosition.z - bound.z, initialPosition.z + bound.z);
        nextMovementPoint = new Vector3(posX, posY, posZ);
    }

    [Task]
    public void SetRandomEscapePoint()
    {
        animator.SetTrigger("surprised");
        AudioManager.Instance.PlayTrack("meep_surprised");
        currentEscapePoint = escapePoints[Random.Range(0, escapePoints.Length)];

        IsSurprised = true;
        Task.current.Succeed();
    }

    Coroutine surprised;
    [Task]
    public void PlaySurprised()
    {
        if(surprised == null)
        {
            surprised = StartCoroutine(SurprisedCoroutine());
        }
    }

    IEnumerator SurprisedCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        IsSeparated = true;
    }

    [Task]
    public void Escape()
    {
        if (Vector3.Distance(currentEscapePoint.position, transform.position) <= 2.0f)
        {
            transform.Translate(Vector3.forward * idleSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(currentEscapePoint.position - transform.position),
                Time.deltaTime);

            if (Vector3.Distance(currentEscapePoint.position, transform.position) <= 1.0f)
                IsWait = true;
        }
        else
        {
            transform.Translate(Vector3.forward * escapeSpeed * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(currentEscapePoint.position - transform.position),
                Time.deltaTime);
        }
            

        Task.current.Succeed();
    }


    [Task]
    public void SetParent()
    {
        transform.SetParent(parent);
        animator.SetTrigger("move");
        IsSeparated = false;
        Task.current.Succeed();
    }

    [Task]
    public void RemoveParent()
    {
        transform.SetParent(null);
        Task.current.Succeed();
    }


    [Task]
    public void Move()
    {
        transform.Translate(Vector3.forward * idleSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation,
            Quaternion.LookRotation(nextMovementPoint - transform.localPosition),
            Time.deltaTime);

        if (Vector3.Distance(nextMovementPoint, transform.localPosition) <= 1.0f)
            CalculateNextMovementPoint();

        Task.current.Succeed();
    }

    [Task]
    public void PlayScaredAnimation()
    {
        animator.SetTrigger("scared");
        Task.current.Succeed();
    }

    [Task]
    bool IsCollected = false;


    [Task]
    public void CheckForPlayer()
    {
        //Debug.Log(Vector3.Distance(parent.position, transform.position));
        if (IsCollected)
        {
            IsWait = false;
            IsScared = false;
            surprised = null;
            IsCollected = false;
            Task.current.Fail();
        }
        Task.current.Succeed();
    }

    [Task]
    public void WaitForCollection()
    {
        if (IsCollected)
        {
            Task.current.Succeed();
        }
    }
          

    [Task]
    public void Reset()
    {
        IsWait = false;
        IsScared = false;
        surprised = null;
        IsCollected = false;
        Task.current.Succeed();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Bell")
        {
            Debug.Log("Collided with beell");
            IsCollected = true;
            AudioManager.Instance.PlayTrack("meep_back");
        }
    }
}
