﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using DG.Tweening;

public class ActivatedCheckpoint : MonoBehaviour
{
    [SerializeField]
    private CheckPoint checkpoint;
    [SerializeField]
    private GameObject effects;
    [SerializeField]
    private UIAnimationController ui;

    [SerializeField]
    private bool isShown, isActivated;

    [SerializeField]
    private Color unactivated, activated;

    private Material effectMaterial;
    private void Awake()
    {
        effectMaterial = effects.GetComponent<Renderer>().material;
        effectMaterial.SetColor("_Color", Color.black);
        //effectMaterial.SetColor("_Color", unactivated);
    }

    private void OnEnable()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && !isShown && !isActivated)
        {
            ui.TransitionIn();
            effectMaterial.DOColor(unactivated, 1.0f);
            isShown = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (Input.GetKeyDown(KeyCode.E) && isShown)
            {
                isActivated = true;
                isShown = false;
                ui.TransitionOut();
                effectMaterial.DOColor(activated, 1.0f);
                CheckPoint current = GameLevelManager.Instance.CurrentCheckPoint;
                if(current.gameObject.GetComponent<ActivatedCheckpoint>() != null)
                {
                    if(current.gameObject.GetComponent<ActivatedCheckpoint>() != this)
                        current.gameObject.GetComponent<ActivatedCheckpoint>().DisableCheckpoint();
                }
                GameLevelManager.Instance.SetCurrentCheckpoint(checkpoint);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!isActivated && isShown)
            {
                ui.TransitionOut();
                effectMaterial.DOColor(Color.black, 1.0f);
                isShown = false;
            }
        }
    }

    public void DisableCheckpoint()
    {
        if (isActivated)
        {
            effectMaterial.DOColor(Color.black, 1.0f);
            isActivated = false;
            isShown = false;

        }
    }
}
