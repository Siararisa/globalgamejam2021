﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;

using MoreMountains.CorgiEngine;

public class GameLevelManager : MoreMountains.CorgiEngine.LevelManager
{
    protected override void Initialization()
    {
        // storage
        LevelCameraController = FindObjectOfType<CameraController>();
        _savedPoints = GameManager.Instance.Points;
        _started = DateTime.UtcNow;

        // if we don't find a bounds collider we generate one
        BoundsCollider = this.gameObject.GetComponent<Collider>();
        if (BoundsCollider == null)
        {
            GenerateColliderBounds();
            BoundsCollider = this.gameObject.GetComponent<Collider>();
        }


        //Checkpoints = FindObjectsOfType<CheckPoint>().ToList();
        CurrentCheckPoint = DebugSpawn;
    }

    /// <summary>
    /// Assigns all respawnable objects in the scene to their checkpoint
    /// </summary>
    protected override void CheckpointAssignment()
    {
        /*
        // we get all respawnable objects in the scene and attribute them to their corresponding checkpoint
        IEnumerable<Respawnable> listeners = FindObjectsOfType<MonoBehaviour>().OfType<Respawnable>();
        foreach (Respawnable listener in listeners)
        {
            for (int i = Checkpoints.Count - 1; i >= 0; i--)
            {
                Checkpoints[i].AssignObjectToCheckPoint(listener);
            }
        }
        */
    }

}
