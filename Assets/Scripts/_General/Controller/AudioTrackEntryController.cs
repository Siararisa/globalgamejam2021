﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioTrackEntryController : MonoBehaviour, IAudioTrackEntry
{
    [SerializeField] AudioSource _audioSource;
    [SerializeField] string _trackName;
    [SerializeField] protected bool willUnregister = false;
    [SerializeField] protected bool checkIsPlaying;
    [SerializeField] protected bool isPaused = false;

    protected float t, _origVolume;
    protected float origVolume { get { return _origVolume; } set { _origVolume = value; } }

    protected bool isFading;
    public bool hasPlayed;
    protected virtual void Awake()
    {
        DontDestroyOnLoad(this);
    }

    protected virtual void OnDisable()
    {
        if (!isPaused)
            Unregister();
    }


    protected virtual void OnEnable()
    {

    }

    public AudioSource audioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = this.GetComponent<AudioSource>();
            return _audioSource;
        }
    }

    public bool isPlaying
    {
        get { return audioSource.isPlaying; }
    }

    protected virtual void Unregister()
    {
        if (!FindObjectOfType(typeof(AudioManager))) return;
        AudioManager.updateMasterVol -= UpdateVolume;
        AudioManager.updateBGMVol -= UpdateVolume;
        AudioManager.updateSFXVol -= UpdateVolume;
        AudioManager.updateAmbientVol -= UpdateVolume;

        AudioManager.muteAllAudio -= Mute;
        AudioManager.muteAllBGM -= Mute;
        AudioManager.muteAllSFX -= Mute;
        AudioManager.muteAllAmbient -= Mute;

        StopAllCoroutines();

        if (!isPersist)
        {
            AudioManager.Instance.UnRegisterTrack(this);
            hasPlayed = false;
            willUnregister = false;
        }

    }

    protected void UpdateVolume(float value)
    {
        audioSource.volume = value * AudioManager.Instance.masterVolume * volumeModifier;
        origVolume = audioSource.volume;
        if (fadeOption.fadeType == FadeType.In)
        {
            audioSource.volume = 0;
        }

    }

    protected void UpdateMasterVolume(float value)
    {
        AudioManager.Instance.masterVolume = value;
        if (AudioManager.Instance.ExistsInRegistry(this))
        {
            switch (audioTrackType)
            {
                case AudioTrackType.BGM:
                    audioSource.volume = (AudioManager.Instance.backgroundMusicVolume * AudioManager.Instance.masterVolume) * volumeModifier;
                    break;
                case AudioTrackType.SFX:
                    audioSource.volume = (AudioManager.Instance.soundEffectsVolume * AudioManager.Instance.masterVolume) * volumeModifier;
                    break;
                case AudioTrackType.Ambient:
                    audioSource.volume = (AudioManager.Instance.ambientSoundVolume * AudioManager.Instance.masterVolume) * volumeModifier;
                    break;
            }
        }



    }
    protected IEnumerator WaitBeforeFadeOut(float sec)
    {
        yield return new WaitForSeconds(sec);
        if (fadeOption.isFixedSpeed)
            StartCoroutine("FixedFadeOut");
        else if (fadeOption.fadeOutDuration != 0 && !fadeOption.isFixedSpeed)
            StartCoroutine("FadeOut", fadeOption.fadeOutDuration);
        else
            StartCoroutine("FadeOut", audioSource.clip.length);
    }
    protected IEnumerator FixedFadeOut()
    {
        while (audioSource.volume > 0f)
        {
            audioSource.volume -= fadeOption.fadeSpeed * Time.deltaTime;
            yield return null;
        }
        Stop();
    }
    protected IEnumerator FadeOut(float duration)
    {
        t = 0;
        while (audioSource.volume > 0f)
        {
            Debug.Log("fading out called");
            audioSource.volume = Mathf.Lerp(origVolume, 0, t);
            if (t < 1)
                t += Time.deltaTime / duration;
            yield return null;
        }
        Stop();
    }
    protected IEnumerator FixedFadeIn()
    {
        float vol = 0;
        while (vol < maxVolume(this))
        {
            vol += fadeOption.fadeSpeed * Time.deltaTime;
            audioSource.volume = vol;
            yield return null;
        }

    }
    protected IEnumerator FadeIn(float duration)
    {
        float vol = 0;
        t = 0;
        while (vol < maxVolume(this))
        {
            vol = Mathf.Lerp(0f, origVolume, t);
            audioSource.volume = vol;
            if (t < 1)
                t += Time.deltaTime / duration;
            yield return null;
        }
    }

    protected float maxVolume(AudioTrackEntryController atec)
    {
        switch (atec.audioTrackType)
        {
            case AudioTrackType.BGM:
                return AudioManager.Instance.backgroundMusicVolume * AudioManager.Instance.masterVolume;
            case AudioTrackType.SFX:
                return AudioManager.Instance.soundEffectsVolume * AudioManager.Instance.masterVolume;
            case AudioTrackType.Ambient:
                return AudioManager.Instance.ambientSoundVolume * AudioManager.Instance.masterVolume;
        }
        return 1;
    }

    protected void Mute(bool foo)
    {
        audioSource.mute = foo;
    }
    public void Stop()
    {
        willUnregister = true;
        audioSource.Stop();
        Unregister();
    }

    #region IAudioTrackEntry implementation
    public string trackName { get { return _trackName; } set { _trackName = value; } }
    public AudioTrackType audioTrackType { get; set; }
    public float volumeModifier { get; set; }
    public bool isLooping { get { return audioSource.loop; } set { audioSource.loop = value; } }
    public bool isPersist { get; set; }
    public FadeOptions fadeOption { get; set; }
    #endregion

    public void InitializeAudioTrack(AudioTrackEntry aTrackEntry)
    {
        trackName = aTrackEntry.trackName;
        audioTrackType = aTrackEntry.audioTrackType;
        volumeModifier = aTrackEntry.volumeModifier;
        isLooping = aTrackEntry.isLooping;
        isPersist = aTrackEntry.isPersist;
        fadeOption = aTrackEntry.fadeOption;

        audioSource.clip = Resources.Load<AudioClip>("Audio/" + aTrackEntry.audioTrackType + "/" + aTrackEntry.trackName);
        AudioManager.updateMasterVol += UpdateVolume;
        switch (audioTrackType)
        {
            case AudioTrackType.BGM:
                MasterVolume();
                //AudioManager.updateMasterVol += UpdateVolume;
                AudioManager.updateBGMVol += UpdateVolume;
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllBGM += Mute;

                FadeSound();
                break;
            case AudioTrackType.SFX:
                //AudioManager.updateMasterVol += UpdateVolume;
                AudioManager.updateSFXVol += UpdateVolume;
                MasterVolume();
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllSFX += Mute;
                FadeSound();
                break;
            case AudioTrackType.Ambient:
                //AudioManager.updateMasterVol += UpdateVolume;
                MasterVolume();
                AudioManager.updateAmbientVol += UpdateVolume;
                AudioManager.muteAllAudio += Mute;
                AudioManager.muteAllAmbient += Mute;
                FadeSound();
                break;
        }

        //audioSource.Play() ;
        willUnregister = false;
    }


    //call this function to fade out music
    public void FadeOutOnStop(float fadeSpeedValue = 1.0f)
    {
        fadeOption.fadeSpeed = fadeSpeedValue;
        StartCoroutine(FixedFadeOut());
    }

    public void SetPersistence(bool value)
    {
        isPersist = value;
    }

    protected void MasterVolume()
    {
        AudioManager.updateMasterVol += UpdateMasterVolume;
    }

    protected void FadeSound()
    {
        switch (fadeOption.fadeType)
        {
            case FadeType.None:
                break;
            case FadeType.In:
                audioSource.volume = 0;
                if (fadeOption.isFixedSpeed)
                    StartCoroutine("FixedFadeIn");
                else if (fadeOption.fadeInDuration != 0)
                    StartCoroutine("FadeIn", fadeOption.fadeInDuration);
                else
                    StartCoroutine("FadeIn", audioSource.clip.length);
                break;
            case FadeType.Out:
                if (!fadeOption.isFadeFromStart)
                    StartCoroutine("WaitBeforeFadeOut", fadeOption.fadeOutStart);
                else if (fadeOption.isFadeFromStart && fadeOption.isFixedSpeed)
                    StartCoroutine("FixedFadeOut");
                else if (fadeOption.isFadeFromStart && !fadeOption.isFixedSpeed && fadeOption.fadeOutDuration != 0)
                    StartCoroutine("FadeOut", fadeOption.fadeOutDuration);
                else
                    StartCoroutine("FadeOut", audioSource.clip.length);
                break;
            case FadeType.Both:
                if (fadeOption.isFixedSpeed)
                    StartCoroutine("FixedFadeIn");
                else if (fadeOption.fadeInDuration != 0)
                    StartCoroutine("FadeIn", fadeOption.fadeInDuration);
                else
                    StartCoroutine("FadeIn", audioSource.clip.length);

                if (!fadeOption.isFadeFromStart)
                    StartCoroutine("WaitBeforeFadeOut", fadeOption.fadeOutStart);
                else if (fadeOption.isFadeFromStart && fadeOption.isFixedSpeed)
                    StartCoroutine("FixedFadeOut");
                else if (fadeOption.isFadeFromStart && !fadeOption.isFixedSpeed && fadeOption.fadeOutDuration != 0)
                    StartCoroutine("FadeOut", fadeOption.fadeOutDuration);
                else
                    StartCoroutine("FadeOut", audioSource.clip.length);
                break;
        }
    }

    protected virtual void Update()
    {
        if (!willUnregister)
        {
            checkIsPlaying = isPlaying;
            if (!isPlaying && !isPaused && hasPlayed)
            {
                Stop();
            }
        }
    }


    public void PauseTrack(bool boo)
    {
        if (boo)
        {
            audioSource.Pause();
            isPaused = true;
        }
        else
        {
            audioSource.Play();
            isPaused = false;
        }
    }
    public void OnApplicationPause(bool boo)
    {
        isPaused = boo;
    }

    public void SetPitch(float value)
    {
        audioSource.pitch = value;
    }

    public void Play()
    {
        audioSource.Play();
        hasPlayed = true;
    }
    public bool IsUnregister()
    {
        return willUnregister;
    }
}
