﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUIController : MonoBehaviour {

    public Canvas debugCanvas;
    public GameObject messagePrefab;
    public Transform debugContent;
    public ScrollRect scrollView;
    public Text messageLog, warningLog, errorLog;
    public GameObject instructions, status;

    [SerializeField]
    private List<GameObject> debugMessages = new List<GameObject>();
    private bool showMessage, showError, showWarning;
    [SerializeField]
    private int messageCount, warningCount, errorCount;

    private CanvasGroup canvasGroup;

    private void Awake()
    {
        canvasGroup = debugCanvas.GetComponent<CanvasGroup>();
        showMessage = true;
        UpdateLogText();
        DebugManager.OnDebugLogMessage += LogDebug;
        DebugManager.OnDebugLogWarning += LogDebug;
        DebugManager.OnDebugLogError += LogDebug;
        DebugManager.OnEnterDebugMode += EnterDebugMode;
        DebugManager.OnClearDebugData += ClearDebugMessages;
    }

    private void OnDestroy()
    {
        DebugManager.OnDebugLogMessage -= LogDebug;
        DebugManager.OnDebugLogWarning -= LogDebug;
        DebugManager.OnDebugLogError -= LogDebug;
        DebugManager.OnEnterDebugMode -= EnterDebugMode;
        DebugManager.OnClearDebugData -= ClearDebugMessages;
    }

    public void EnterDebugMode(bool enabled)
    {
        if (debugCanvas != null)
            debugCanvas.gameObject.SetActive(enabled);
        UpdateCanvas();
    }

    public void LogDebug(DebugData data)
    {
        DebugMessage(data);
    }

    private void DebugMessage(DebugData data)
    {
        GameObject logObject = ObjectPoolManager.Instance.GetPooledObjectByID("DebugMessage");
        if (logObject != null)
        {
            logObject.transform.SetParent(debugContent);
            logObject.GetComponent<DebugDataUI>().SetDebugData(data);
            AddToDebugList(logObject);
        }
    }

    private void AddToDebugList(GameObject data)
    {
        if(debugMessages.Count >= ObjectPoolManager.Instance.GetPooledObjectData("DebugMessage").amountToPool - 1)
        {
            debugMessages[0].SetActive(false);
            debugMessages[0].transform.SetParent(ObjectPoolManager.Instance.GetParentOfObjectID("DebugMessage"));
            debugMessages.RemoveAt(0);
            DebugManager.Instance.RemoveOldestDebugData();
        }  
        debugMessages.Add(data);
        IncreaseLogCount(data.GetComponent<DebugDataUI>().type);
        ToggleDebugMessages();
    }

    private void IncreaseLogCount(MessageType data)
    {    
        switch (data)
        {
            case MessageType.message:
                if(messageCount < ObjectPoolManager.Instance.GetPooledObjectData("DebugMessage").amountToPool)
                    messageCount++;
                break;
            case MessageType.warning:
                if (warningCount < ObjectPoolManager.Instance.GetPooledObjectData("DebugMessage").amountToPool)
                    warningCount++;
                break;
            case MessageType.error:
                if(errorCount < ObjectPoolManager.Instance.GetPooledObjectData("DebugMessage").amountToPool)
                    errorCount++;
                break;
        }
        UpdateLogText();
    }
    public void ShowMessages(bool isEnabled)
    {
        showMessage = isEnabled;
        ToggleDebugMessages();
    }

    public void ShowWarnings(bool isEnabled)
    {
        showWarning = isEnabled;
        ToggleDebugMessages();
    }

    public void ShowErrors(bool isEnabled)
    {
        showError = isEnabled;
        ToggleDebugMessages();
    }

    void ToggleDebugMessages()
    {
        for (int i = 0; i < debugMessages.Count; i++)
        {
            switch (debugMessages[i].GetComponent<DebugDataUI>().type)
            {
                case MessageType.message:
                    debugMessages[i].gameObject.SetActive(showMessage);
                    break;
                case MessageType.warning:
                    debugMessages[i].gameObject.SetActive(showWarning);
                    break;
                case MessageType.error:
                    debugMessages[i].gameObject.SetActive(showError);
                    break;
            }
        }
        UpdateLogText();
        UpdateCanvas();
    }

    private void UpdateCanvas()
    {
        Canvas.ForceUpdateCanvases();
        debugContent.GetComponent<VerticalLayoutGroup>().CalculateLayoutInputVertical();
        debugContent.GetComponent<ContentSizeFitter>().SetLayoutVertical();
        scrollView.content.GetComponent<VerticalLayoutGroup>().CalculateLayoutInputVertical();
        scrollView.content.GetComponent<ContentSizeFitter>().SetLayoutVertical();
        scrollView.verticalNormalizedPosition = 0;
    }

    public void ClearDebugMessages()
    {
        for(int i = 0; i < debugMessages.Count; i++)
        {
            debugMessages[i].SetActive(false);
            debugMessages[i].transform.SetParent(ObjectPoolManager.Instance.GetParentOfObjectID("DebugMessage"));
        }
        debugMessages.Clear();
        warningCount = 0;
        messageCount = 0;
        errorCount = 0;
        UpdateLogText();
    }

    private void UpdateLogText()
    {
        messageLog.text = string.Format("Messages [{0}]", messageCount);
        warningLog.text = string.Format("Warning [{0}]", warningCount);
        errorLog.text = string.Format("Error [{0}]", errorCount);
    }

    public void ShowInstructions(bool boo)
    {
        instructions.SetActive(boo);
        status.SetActive(!boo);
    }

    public void AdjustOpacity(float value)
    {
        float alpha = Mathf.Lerp(0.30f, 1.0f, value);
        canvasGroup.alpha = alpha;
    }
}
