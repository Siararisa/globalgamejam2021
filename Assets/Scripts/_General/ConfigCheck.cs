﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.IO;
using System;

public class ConfigCheck : Singleton<ConfigCheck> {

	// Use this for initialization
	public static XmlDocument ConfigFile;
	const string ConfigFileName = "AppConfig.xml";
	public string ConfigPath;

    public static string deviceID;
    public static string storeID;
    public static string computerID;
    public static string googleLoggingURL, googleAnalyticsURL;
    public static bool isRemoteLoggingEnabled;


    public static float viewTime;
    public static float gameTime;
    public static int memoryCardPairs;


    protected override void Awake () {
        base.Awake();
		ConfigPath = Application.dataPath+ "/../"+ConfigFileName;
		CheckConfig();
	}

	void CheckConfig()
	{
		if (File.Exists(ConfigPath) == false)
			CreateConfig();
		
		ReadConfig();
	}

	void CreateConfig(){
		using (StreamWriter sw = File.CreateText(ConfigPath))
		{
			sw.WriteLine(@"<?xml version=""1.0"" encoding=""utf-8"" ?>");
			sw.WriteLine("<root>");

            sw.WriteLine("  <storeID>");
            sw.WriteLine("    <string>HTECH</string> <!-- If empty use default device id -->");
            sw.WriteLine("  </storeID>");

            sw.WriteLine("  <computerName>");
            sw.WriteLine("    <string>HTECH_01</string> <!-- If empty use default machine name -->");
            sw.WriteLine("  </computerName>");

            sw.WriteLine("  <RemoteLogging>");
            sw.WriteLine("    <enabled>true</enabled>");
            sw.WriteLine("    <loggingUrl>https://script.google.com/macros/s/AKfycbzMadC3khWTGezm6n7h5g0oiwbLBD-5qryE8Z_ZRw/exec</loggingUrl>");
            sw.WriteLine("    <analyticsUrl>https://script.google.com/macros/s/AKfycbx72aAx5WgD8ys5L8Yo0ydPnFwQq88dBsIAhhPc0U3KDjAOWTHu/exec</analyticsUrl>");
            sw.WriteLine("  </RemoteLogging>");

 
            sw.WriteLine("  <MemoryGame>");
            sw.WriteLine("    <ViewTime>5</ViewTime>");
            sw.WriteLine("    <GameTime>60</GameTime>");
            sw.WriteLine("    <MemoryCardPairs>8</MemoryCardPairs>");
     
            sw.WriteLine("  </MemoryGame>");


            sw.WriteLine("</root>");
		}
	}

    public string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        string projectName = s[s.Length - 2];
        Debug.Log("project = " + projectName);
        return projectName;
    }

    void ReadConfig()
	{
		ConfigFile = new XmlDocument();
		ConfigFile.Load(ConfigPath);

        storeID = ConfigFile.DocumentElement["storeID"].ChildNodes[0].InnerText.ToString();
        computerID = ConfigFile.DocumentElement["computerName"].ChildNodes[0].InnerText.ToString();

        isRemoteLoggingEnabled = bool.Parse(ConfigFile.DocumentElement["RemoteLogging"].ChildNodes[0].InnerText);
        googleLoggingURL = ConfigFile.DocumentElement["RemoteLogging"].ChildNodes[1].InnerText.ToString();
        googleAnalyticsURL = ConfigFile.DocumentElement["RemoteLogging"].ChildNodes[2].InnerText.ToString();
        CheckDeviceName();

      
        viewTime = float.Parse(ConfigFile.DocumentElement["MemoryGame"].ChildNodes[0].InnerText);
        gameTime = int.Parse(ConfigFile.DocumentElement["MemoryGame"].ChildNodes[1].InnerText);
        memoryCardPairs = int.Parse(ConfigFile.DocumentElement["MemoryGame"].ChildNodes[2].InnerText);

    }

    private void CheckDeviceName()
    {
        if (storeID.Trim() == "")
        {
            storeID = SystemInfo.deviceUniqueIdentifier;
            Debug.Log("Default device indentifier is used!");
        }
        else
        {
            if (computerID.Trim() == "")
            {
                computerID = Environment.MachineName.ToString();
                Debug.Log("Default computer name is used : " + computerID);
            }
            else
            {
                Debug.Log("Computer name is not null : " + computerID);
            }

        }

        deviceID = storeID + "_" + computerID;
    }
}
