﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WindowManager : Singleton<WindowManager> {

	public delegate void controlWindow(string windowName);

	public WindowEntryController initialWindow;
	[SerializeField] private WindowEntryController currentWindow;

	public bool PlayOnAwake;

	public List<WindowEntryController> windowControllers;//window controllers must be assigned here via inspector
	public List<WindowEntryController> orderedWindows;//the order of windows that were opened; history; used for the arrangement of windows when going back

	public static event controlWindow OnTransitionIn;
	public static event controlWindow OnTransitionOut;

	private void TransitionIn(string windowName){if (OnTransitionIn != null)OnTransitionIn (windowName);}
	private void TransitionOut(string windowName){if (OnTransitionOut != null)OnTransitionOut (windowName);}

	private float transitionOutDelay, transitionInDelay;
	public bool isTransitioningOut;
	private bool _isTransitioningIn;
	public bool isTransitioningIn{ get{return _isTransitioningIn;} set{ _isTransitioningIn = value; } }


	protected override void Awake(){
        base.Awake();
        FindWindows();
        StartCoroutine(Initialize());
		
       
		if (PlayOnAwake) {
			TransitionInWindow (initialWindow.windowName);
		}
	}

    private IEnumerator Initialize()
    {
        bool isComplete = false;

        if (initialWindow == null)
            initialWindow = windowControllers[0];
        if (currentWindow == null)
            currentWindow = initialWindow;

        while (!isComplete)
        {
            yield return null;
            //windows need to be enabled at start for values to be initialized
            foreach (WindowEntryController wec in windowControllers)
            {
            
                wec.gameObject.SetActive(true);
                if (wec.transitionAnimation != null)
                {
                    wec.transitionAnimation.Initialize();
                    if (wec.gameObject.GetComponent<GroupedUIAnimationController>() != null)
                        wec.gameObject.GetComponent<GroupedUIAnimationController>().InitializeUI();
                }
            }
            isComplete = true;
        }

        //when all values are initialized
        //only open the initial window at the beginning
        foreach (WindowEntryController wec in windowControllers)
        {
            if (wec != initialWindow)
            {
                wec.gameObject.SetActive(false);
            }

        }
    }

    void FindWindows()
    {
        if (windowControllers.Count < 0)
        {
            WindowEntryController[] wec = FindObjectsOfType<WindowEntryController>();
            windowControllers = wec.ToList<WindowEntryController>();
        }
    }

	public void RegisterWindows()//use this method if values are not assigned in the inspector
	{
		WindowEntryController[] controllerArray = Resources.FindObjectsOfTypeAll<WindowEntryController> ();
		List<WindowEntryController> controllers = controllerArray.ToList();
		foreach (WindowEntryController wec in controllers) {
			windowControllers.Add (wec);
		}
	}
		
	public void TransitionInWindow(string windowName){
		WindowEntryController window = FindWindow (windowName);//check if the window exists
        if (window == null)
        {
            Debug.Log("window with " + windowName + " not found!");
            return;
        }
		if (!window.doNotRegister)//check if window should be registered (most windows that are not registered are overlays)
			currentWindow = window;//make the window the current window
		if (currentWindow == initialWindow)
			orderedWindows.Clear ();//clear the history of windows because we are at the "home" page
		isTransitioningIn = true;
		//window.TransitionIn ();//call the transition of window
		StartCoroutine ("TransitionInDelay", window);
	}
		
	public void TransitionOutWindow(string windowName){
		ResetDelay ();
		WindowEntryController window = FindWindow(windowName);
        if (window == null)
        {

            Debug.Log("window with " + windowName + " not found!");
            return;
        }
		TransitionOut (window.windowName);
		isTransitioningOut = true;
		StartCoroutine ("TransitionOutDelay", window);
	}

	private void ResetDelay(){
		transitionOutDelay = 0;
	}

    public void TransitionOutAllActiveWindows()
    {
        for(int i = 0; i < windowControllers.Count; i++)
        {
            if (windowControllers[i].gameObject.activeInHierarchy)
                TransitionOutWindow(windowControllers[i].windowName);
        }
    }
		
	//wait for the transition out to finish before transitioning in
	private IEnumerator TransitionInDelay(WindowEntryController window){
		yield return new WaitForSeconds (transitionOutDelay);
		window.TransitionIn ();//call the transition of window
		//AddOrderedWindows(window);//register the window to the history after being transitioned
		TransitionIn (window.windowName);
	}

	//wait the transition out of ui groups before window transitions out
	IEnumerator TransitionOutDelay(WindowEntryController window){
		yield return new WaitForSeconds (transitionOutDelay);
		window.TransitionOut ();
		isTransitioningOut = false;
	}


	public void CurrentWindowTransitionOut(){
		currentWindow.TransitionOut ();
		orderedWindows.Add (currentWindow);
	}

	private void AddOrderedWindows(WindowEntryController window){
		if (orderedWindows.Count > 0) {//check first if there are windows registered in the history
			bool isExisting = false;//check if the window has been registered before registering
			foreach (WindowEntryController win in orderedWindows) {
				if (win.windowName == window.windowName)
					isExisting = true;
				else 
					isExisting = false;
			}
			if (!isExisting) {
				orderedWindows.Add (window);
			}
		} else {//if there are no windows in the history, add 
			orderedWindows.Add (window);
		}
	}

	//use this to transition out current window and transition in last window visited
	public void BackWindow(){
		if (orderedWindows.Count > 0) {
			if(currentWindow == null)
				return;
			if (currentWindow == initialWindow)
				return;
			orderedWindows.RemoveAt (orderedWindows.Count - 1);
			TransitionOutWindow (currentWindow.windowName);
			TransitionInWindow(orderedWindows[orderedWindows.Count-1].windowName);
		}
	}
	private WindowEntryController FindWindow(string windowName){
		WindowEntryController wec = windowControllers.Find (window => window.windowName == windowName);
		if (wec != null)
			return wec;
		else
			return null;
	}
		
	public void CloseAllWindows(){
		foreach (WindowEntryController wec in windowControllers) {
			TransitionOutWindow (wec.name);
		}
	}

	public void SetTransitionOutDelay(float value){
		transitionOutDelay = value;
	}

	public void SetTransitionInDelay(float value){
		transitionOutDelay = value;
	}
		
	public bool IsTransitioning(){
		return (isTransitioningOut || isTransitioningIn);
	}

	public float GetTransitionOutDelay(){
		return transitionOutDelay;
	}

	public WindowEntryController GetCurrentWindow(){
		return currentWindow;
	}

    public WindowEntryController GetWindow(string id)
    {
       return FindWindow(id);
    }

    public bool IsWindowActive(string windowName)
    {
        WindowEntryController window = FindWindow(windowName);//check if the window exists
        if (window == null)
        {
            Debug.Log("window with " + windowName + " not found!");
            return false;
        }
        return window.transitionAnimation.transitionInAnimation.targetGameObject.activeInHierarchy;
    }
}