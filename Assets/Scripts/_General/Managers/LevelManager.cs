﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Level {Init, MainMenu, DataRescue, MemoryGame, ModernHero, Selfie, GlobeOne}

public class LevelManager : Singleton<LevelManager> {

	public delegate void levelLoad (Scene scene, LoadSceneMode sceneMode);
	public static event levelLoad LevelLoaded;

	public delegate void levelUnload(Scene Scene);
	public static event levelUnload LevelUnloaded;

	public delegate void activeLevelChange(Scene previousActiveScene, Scene currentActiveScene);
	public static event activeLevelChange ActiveLevelChanged;

    AsyncOperation async;

	private float loadDelay = 0.25f;

	void OnEnable(){
		SceneManager.sceneLoaded += OnLevelLoaded;
		SceneManager.sceneUnloaded += OnLevelUnloaded;
		SceneManager.activeSceneChanged += OnActiveLevelChanged;
	}

	void OnDisable(){
		SceneManager.sceneLoaded -= OnLevelLoaded;
		SceneManager.sceneUnloaded -= OnLevelUnloaded;
		SceneManager.activeSceneChanged -= OnActiveLevelChanged;
	}

    private void Start()
    {
        //ArduinoController.Instance.CommunicateWithArduino();
    }
    public void OnLevelLoaded(Scene scene, LoadSceneMode sceneMode){
		if (LevelLoaded != null) {
			LevelLoaded(scene, sceneMode);
		}
	}

	public void OnLevelUnloaded(Scene scene){
		if (LevelUnloaded != null) {
			LevelUnloaded (scene);
		}
	}

	public void OnActiveLevelChanged(Scene previous, Scene current){
		if (ActiveLevelChanged != null) {
			ActiveLevelChanged (previous, current);
		}
	}

    public void AsyncLoadLevel(string levelName)
    {
        StartCoroutine("AsyncLoadLevelCoroutine", levelName);
    }

    public void AsyncLoadLevel(Level levelType)
    {
        StartCoroutine("AsyncLoadLevelCoroutine", levelType);
    }

    public void AllowAsyncLevelToLoad()
    {
        ActivateScene();
    }

	public void LoadLevel(int levelIndex){
		StartCoroutine("LoadLevelCoroutine", levelIndex);
	}

	public void LoadLevel(string levelName){
		StartCoroutine("LoadLevelCoroutine", levelName);
	}

	public void LoadLevel(Level level){
		StartCoroutine("LoadLevelCoroutine", level);
	}

	public void LoadLevel(int levelIndex, float delay){
		SetLoadDelay (delay);
		StartCoroutine("LoadLevelCoroutine", levelIndex);
	}

	public void LoadLevel(string levelName, float delay){
		SetLoadDelay (delay);
		StartCoroutine("LoadLevelCoroutine", levelName);
	}

	public void LoadLevel(Level level, float delay){
		SetLoadDelay (delay);
		StartCoroutine("LoadLevelCoroutine", level);
	}

	private IEnumerator LoadLevelCoroutine(int levelIndex){
		yield return new WaitForSeconds (loadDelay);
		SceneManager.LoadScene (levelIndex);
	}

	private IEnumerator LoadLevelCoroutine(string levelName){
		yield return new WaitForSeconds (loadDelay);
		SceneManager.LoadScene (levelName);
	}

	private IEnumerator LoadLevelCoroutine(Level level){
		yield return new WaitForSeconds (loadDelay);
		SceneManager.LoadScene ((int) level);
	}

	public void SetLoadDelay(float value){
		//Debug.Log ("delay is " + value);
		loadDelay = value;
	}

    IEnumerator AsyncLoadLevelCoroutine(string levelName)
    {
        Debug.LogWarning("ASYNC LOAD STARTED - " +
           "DO NOT EXIT PLAY MODE UNTIL SCENE LOADS... UNITY WILL CRASH");
        async = SceneManager.LoadSceneAsync(levelName);
        async.allowSceneActivation = false;
        yield return async;
    }

    IEnumerator AsyncLoadLevelCoroutine(Level levelType)
    {
        Debug.LogWarning("ASYNC LOAD STARTED - " +
           "DO NOT EXIT PLAY MODE UNTIL SCENE LOADS... UNITY WILL CRASH");
        async = SceneManager.LoadSceneAsync((int)levelType);
        async.allowSceneActivation = false;
        yield return async;
    }


    public void ActivateScene()
    {
        async.allowSceneActivation = true;
    }

    public bool IsAsyncReady() {
        return (async.progress >= 0.9f);
    }
   

}
