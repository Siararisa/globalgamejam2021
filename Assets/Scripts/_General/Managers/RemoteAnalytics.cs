﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class RemoteAnalytics : Singleton<RemoteAnalytics>
{
    private Queue<string> logs = new Queue<string>();

    const string _DEVICEID = "?deviceId=";
    const string _SENTDATE = "&sentDate=";
    const string _EVENTKEY = "&eventKey=";

    private void OnEnable()
    {
        StartCoroutine(WaitForRequest());
    }

    public void AddEvent(string message)
    {
        Debug.Log("QUEUEING: " + message);
        logs.Enqueue(message);
    }

    IEnumerator WaitForRequest()
    {
        while (true)
        {
            if (logs.Count > 0)
            {
                Debug.Log(ConfigCheck.googleAnalyticsURL);
                var url = new StringBuilder(ConfigCheck.googleAnalyticsURL).Append(_DEVICEID).Append(ConfigCheck.deviceID);
                url.Append(_SENTDATE).Append(System.DateTime.Now.ToString("MM/dd/yyyy"));
                url.Append(_EVENTKEY).Append(UrlEncode(logs.Dequeue()));

                Debug.Log("ANALYTICS SENDING: " + url.ToString());

                UnityWebRequest www = UnityWebRequest.Get(url.ToString());
                yield return www.SendWebRequest();

                www.Dispose();
            }

            yield return null;
        }
    }

    string UrlEncode(string instring)
    {
        StringReader strRdr = new StringReader(instring);
        StringWriter strWtr = new StringWriter();
        int charValue = strRdr.Read();
        while (charValue != -1)
        {
            if (((charValue >= 48) && (charValue <= 57)) || ((charValue >= 65) && (charValue <= 90)) || ((charValue >= 97) && (charValue <= 122)))
                strWtr.Write((char)charValue);
            else if (charValue == 32)
                strWtr.Write("+");
            else
                strWtr.Write("%{0:x2}", charValue);

            charValue = strRdr.Read();
        }
        return strWtr.ToString();
    }
}
