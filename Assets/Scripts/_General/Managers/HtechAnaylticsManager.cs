﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class HtechAnaylticsManager : Singleton<HtechAnaylticsManager> {

    #region Constant Fields
    private const string defaultDirectoryPath = @"C:\HTech\Whack-A-Fox\";
    private const string defaultFileName = "data.txt";
    #endregion

    #region Private Fields
    [Header("Raw Data Settings")]
    [SerializeField]
    private RawSettings rawDataSettings = new RawSettings();

    [Header("Processed Data Settings")]
    [SerializeField]
    private ProcessedSettings processedDataSettings = new ProcessedSettings();

    private string jsonRawData;
    #endregion

    #region Get Set Methods
    public RawSettings RawDataSettings
    {
        get { return rawDataSettings; }
        set { rawDataSettings = value; }
    }

    public ProcessedSettings ProcessedDataSettings
    {
        get { return processedDataSettings; }
        set { processedDataSettings = value; }
    }
    #endregion

    #region Monobehaviour Methods
    // Use this for initialization
    void Start() {
        jsonRawData = readJson(rawDataSettings.getAbsoluteFilePath());
    }
    #endregion

    #region Insert Analytics Method Here
    
    public void OnDataLeap()
    {
        saveTime("DataLeapGameEvent");
    }

    public void OnSelfie()
    {
        saveTime("SelfieEvent");
    }

    public void OnSelfiePhoto()
    {
        saveTime("SelfiePhotoEvent");
    }

    public void OnSelfieGotQR()
    {
        saveTime("SelfieGotQREvent");
    }

    public void OnEasterEggFound()
    {
        saveTime("EasterEggFound");
    }

    public void OnEggCracked()
    {
        saveTime("EasterEggCracked");
    }

    public void OnGie()
    {
        saveTime("GieEvent");
    }

    public void OnGlobeOne()
    {
        saveTime("GlobeOneEvent");
    }

    public void OnLeapWin()
    {
        saveTime("DataLeapWin");
    }

    public void OnLeapLose()
    {
        saveTime("DataLeapLose");
    }

    public void OnSelfServicePayment()
    {
        saveTime("SelfServicePaymentEvent");
    }

    public void OnExpressAssistance()
    {
        saveTime("OnExpressAssistance");
    }

    public void OnToss()
    {
        saveTime("OnTossGame");
    }

    public void OnTossWin()
    {
        saveTime("OnTossWin");
    }

    public void OnTossLose()
    {
        saveTime("OnTossLose");
    }

    public void OnWassup()
    {
        saveTime("OnWassupGame");
    }

    public void OnWassupWin()
    {
        saveTime("OnWassupWin");
    }

    public void OnWassupLose()
    {
        saveTime("OnWassupLose");
    }

    public void OnWifiQMS()
    {
        saveTime("OnWifiQMS");
    }

    public void OnMemoryGame()
    {
        saveTime("OnMemoryGame");
    }

    public void OnMemoryGameWin()
    {
        saveTime("OnMemoryGameWin");
    }

    public void OnMemoryGameLose()
    {
        saveTime("OnMemoryGameLose");
    }

    public void OnDataRescue()
    {
        saveTime("OnDataRescue");
    }

    public void OnDataRescueWin()
    {
        saveTime("OnDataRescueWin");
    }

    public void OnDataRescueLose()
    {
        saveTime("OnDataRescueLose");
    }

    public void OnModernHeroes()
    {
        saveTime("OnModernHeroes");
    }

    public void OnModernHeroesVideoPlayed()
    {
        saveTime("OnModernHeroesVideoPlayed");
    }

    /*
    public void OnSelectedCharity(string charityName)
    {
        SaveEventData("SelectedCharity", new Dictionary<string, string> {
            {"name", charityName},
            {"time", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }
    */


    private void saveTime(string eventName) {
        SaveEventData(eventName, new Dictionary<string, string> {
            { "DateTime", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }
    #endregion

    #region Private Methods
    private void SaveEventData(string eventName, Dictionary<string, string> dict) {
        String json = jsonRawData;

        Dictionary<string, List<Dictionary<string, string>>> jsonDict = (String.IsNullOrEmpty(json)) ?
            new Dictionary<string, List<Dictionary<string, string>>>()
            :
            JsonConvert.DeserializeAnonymousType(json, new Dictionary<string, List<Dictionary<string, string>>>());

        getEventNameDict(jsonDict, eventName).Add(dict);

        saveJson(jsonDict, rawDataSettings.DirectoryPath, rawDataSettings.FileName);

        if (rawDataSettings.MakeCopyFilteredByDate)
            SaveEventDataFilteredDate(eventName, dict);

        jsonRawData = JsonConvert.SerializeObject(jsonDict);

        CountEvents(processedDataSettings.Data);
    }

    private void SaveEventDataFilteredDate(string eventName, Dictionary<string, string> dict)
    {
        string fileName = DateTime.Now.ToString("MM-dd-yyyy") + ".json";
        String json = readJson(rawDataSettings.DirectoryPath + fileName);

        Dictionary<string, List<Dictionary<string, string>>> jsonDict = (String.IsNullOrEmpty(json)) ?
            new Dictionary<string, List<Dictionary<string, string>>>()
            :
            JsonConvert.DeserializeAnonymousType(json, new Dictionary<string, List<Dictionary<string, string>>>());

        getEventNameDict(jsonDict, eventName).Add(dict);

        saveJson(jsonDict, rawDataSettings.DirectoryPath, fileName);
    }

    private List<Dictionary<string, string>> getEventNameDict(Dictionary<string, List<Dictionary<string, string>>> dict, string eventName)
    {
        if (!dict.ContainsKey(eventName))
        {
            dict.Add(eventName, new List<Dictionary<string, string>>());
        }

        return dict[eventName];

    }

    private void CountEvents(List<AnalyzeData> data) {
        String json = jsonRawData;

        Dictionary<string, List<Dictionary<string, string>>> jsonDict = (String.IsNullOrEmpty(json)) ?
            new Dictionary<string, List<Dictionary<string, string>>>()
            :
            JsonConvert.DeserializeAnonymousType(json, new Dictionary<string, List<Dictionary<string, string>>>());

        Dictionary<string, Dictionary<string, string>> dict = new Dictionary<string, Dictionary<string, string>>();
        dict.Add(processedDataSettings.AppName, new Dictionary<string, string>());

        foreach (AnalyzeData datum in data) {
            if (jsonDict.ContainsKey(datum.EventName)) {
                dict[processedDataSettings.AppName].Add("No of " + datum.EventName, jsonDict[datum.EventName].Count.ToString());
            }
            
        }

        saveJson(dict, processedDataSettings.DirectoryPath, processedDataSettings.FileName);

    }

    private void saveJson(object obj, 
        string path,
        string filename)
    {
        createDirectoryIfNotExist(path);
        // serialize JSON to a string and then write string to a file
        File.WriteAllText(path + filename, JsonConvert.SerializeObject(obj, Formatting.Indented));
    }

    private String readJson(string filename)
    {
        try
        {
            // Deserialize file to string
            return File.ReadAllText(filename);
        }
        catch (Exception)
        {
            // If file doesn't exist
            return "";
        }

    }

    private void createDirectoryIfNotExist(string path)
    {
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

    }
    #endregion

    #region Public Class
    [Serializable]
    public class Settings
    {
        [SerializeField]
        [Tooltip("The default directory path is " + defaultDirectoryPath)]
        private string directoryPath = defaultDirectoryPath;
        [SerializeField]
        [Tooltip("The default file name is " + defaultFileName)]
        private string fileName = defaultFileName;

        public string DirectoryPath {  get { return directoryPath; } set { directoryPath = value; } }
        public string FileName { get { return fileName; } set { fileName = value; } }

        public string getAbsoluteFilePath() {
            return getValue(DirectoryPath, defaultDirectoryPath) + getValue(FileName, defaultFileName);
        }

        private string getValue(string value, string defaultValue) {
            return (String.IsNullOrEmpty(value)) ? defaultValue : value;
        }
    }

    [Serializable]
    public class RawSettings : Settings
    {
        [SerializeField]
        private bool makeCopyFilteredByDate;

        public bool MakeCopyFilteredByDate { get { return makeCopyFilteredByDate; } set { makeCopyFilteredByDate = value; } }
    }

    [Serializable]
    public class ProcessedSettings : Settings
    {
        [SerializeField]
        private String appName;
        [SerializeField]
        private List<AnalyzeData> data;

        public String AppName { get { return appName; } set { appName = value; } }
        public List<AnalyzeData> Data { get { return data; } set { data = value; } }
    }

    [Serializable]
    public class AnalyzeData
    {
        [SerializeField]
        private string eventName;
        [SerializeField]
        private string keyName;

        public string EventName { get { return eventName; } set { eventName = value; } }
        public string KeyName { get { return keyName; } set { keyName = value; } }

        public AnalyzeData(string eventName, string keyName) {
            this.eventName = eventName;
            this.keyName = keyName;
        }
    }
    #endregion

}
