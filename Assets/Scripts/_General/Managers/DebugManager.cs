﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Security.Cryptography;
using System.Text;
using System;

public enum MessageType { message, warning, error}
[Serializable]
public class DebugData
{
    public MessageType type;
    public string message;

    public DebugData(string message, MessageType type = MessageType.message)
    {
        this.message = message;
        this.type = type;
    }
}

public class DebugManager : Singleton<DebugManager> {
    public bool showUnityDebug;
    public bool isToggled;

    public delegate void DebugStatus(bool enabled);
    public static event DebugStatus OnEnterDebugMode;

    public delegate void DebugMessage(DebugData data);
    public static event DebugMessage OnDebugLogMessage;
    public static event DebugMessage OnDebugLogWarning;
    public static event DebugMessage OnDebugLogError;

    public delegate void DebugAction();
    public static event DebugAction OnClearDebugData;


    public List<KeyCode> canvasKeyCombination = new List<KeyCode>();
    public List<DebugData> debugMessageData = new List<DebugData>();

    private List<KeyCode> pressedKey = new List<KeyCode>();

    private bool isActivated;
    

    void Start () {
        EnterDebugMode(false);
    }

    void Update()
    {
        CheckCanvasToggler();
        if (isToggled)
        {
            Debugger();
            KinectDebugger();
        }
    }
       

    private void Debugger()
    {
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
                DeletePrefs();
        }
    }

    private void CheckCanvasToggler()
    {
        if (Input.anyKey)
        {
            if (!pressedKey.Contains(Utilities.DetectPressedKeyOrButton()))
                if (Utilities.DetectPressedKeyOrButton() != KeyCode.None)
                {
                    pressedKey.Add(Utilities.DetectPressedKeyOrButton());
                    isActivated = true;
                    for (int i = 0; i < canvasKeyCombination.Count; i++)
                    {
                        if (!pressedKey.Contains(canvasKeyCombination[i]))
                            isActivated = false;
                    }
                    if (isActivated)
                    {
                        isToggled = !isToggled;
                        isActivated = false;
                    }
                    EnterDebugMode(isToggled);
                }
        }
        else
        {
            pressedKey.Clear();
        }
    }

    public void EnterDebugMode(bool enabled)
    {
        if (OnEnterDebugMode != null)
            OnEnterDebugMode(enabled);
        //Cursor.visible = enabled; 
    }

    public void LogMessage(string message)
    {
        RemoteLog.Instance.Log(message);
        DebugData data = new DebugData(message);
        data.message = System.DateTime.Now.ToString("hh:mm:ss tt") + " " + data.message;
        debugMessageData.Add(data);

        if (showUnityDebug)
            Debug.Log(message);

        if (OnDebugLogMessage != null)
            OnDebugLogMessage(data);
    }

    public void LogWarning(string message)
    {
        DebugData data = new DebugData(message, MessageType.warning);
        data.message = System.DateTime.Now.ToString("HH:mm:ss") + " " + data.message;
        debugMessageData.Add(data);

        if (showUnityDebug)
            Debug.LogWarning(message);

        if (OnDebugLogWarning != null)
            OnDebugLogWarning(data);   
    }

    public void LogError(string message)
    {
        DebugData data = new DebugData(message, MessageType.error);
        data.message = System.DateTime.Now.ToString("HH:mm:ss") + " " + data.message;
        debugMessageData.Add(data);

        if (showUnityDebug)
            Debug.LogError(message);

        if (OnDebugLogError != null)
            OnDebugLogError(data);
    }

    public void RemoveOldestDebugData()
    {
        debugMessageData.RemoveAt(0);
    }

    public void ClearAllDebugData()
    {
        debugMessageData.Clear();
        if (OnClearDebugData != null)
            OnClearDebugData();
    }

    private void DeletePrefs()
    {
        PlayerPrefs.DeleteAll();
        LogMessage("PlayerPrefs Deleted!");
    }

    public string CreateHashKey()
    {
        byte[] encodedBytes = new UTF8Encoding().GetBytes(DateTime.Now.ToString("h:mm:ss tt"));
        byte[] hashBytes = MD5.Create().ComputeHash(encodedBytes);
        return BitConverter.ToString(encodedBytes);
    }

    private void KinectDebugger()
    {
      
    }

 
}
