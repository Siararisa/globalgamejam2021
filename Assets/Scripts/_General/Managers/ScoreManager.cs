﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : Singleton<ScoreManager> {

    public delegate void Scored();
    public static event Scored OnScore;
    public static event Scored OnBadScore;

    public delegate void ScoreText(int value);
    public static event ScoreText OnScoreUpdate;

    public int score
    {
        get { return _score; }
        set
        {
            _score = value;
            if (_score > highScore)
            {
                highScore = _score;
            }
        }
    }

    private int _score = 0;
    private int highScore = 0;

    public void ResetScore()
    {
        score = 0;
        Score();
    }

    public void ResetHighScore()
    {
        highScore = 0;
    }

    public void ResetAll()
    {
        score = 0;
        highScore = 0;
    }

    public void AddScore(int value)
    {
        score += value;
        Score();
        ScoreUpdate(score);
    }

    public int GetScore(){ return score;}

    public void Score()
    {
        if (OnScore != null)
            OnScore();
    }

    public void BadScore()
    {
        if (OnBadScore != null)
            OnBadScore();
    }

    public void ScoreUpdate(int value)
    {
        if (OnScoreUpdate != null)
            OnScoreUpdate(value);
    }
}
