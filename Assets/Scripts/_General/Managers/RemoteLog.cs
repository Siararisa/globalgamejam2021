﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using UnityEngine.Networking;

public class RemoteLog : Singleton<RemoteLog>
{
    private Queue<string> logs = new Queue<string>();
    private int maxParams = 10;

    const string _DEVICEID = "?deviceId=";
    const string _SENTDATE = "&sentDate=";
    const string _SENTTIME = "&sentTime=";
    const string _MESSAGE = "&message=";

    private void OnEnable()
    {
        StartCoroutine(WaitForRequest());
    }

    public void Log(string message)
    {
        Debug.Log("QUEUEING: " + message);
        logs.Enqueue(message);
    }

    IEnumerator WaitForRequest()
    {
        while (true)
        {
            if (logs.Count > 0)
            {
                int count = logs.Count > maxParams ? maxParams : logs.Count;

                var url = new StringBuilder(ConfigCheck.googleLoggingURL).Append(_DEVICEID).Append(ConfigCheck.deviceID);
                url.Append(_SENTDATE).Append(System.DateTime.Now.ToString("MM/d/yyyy"));
                url.Append(_SENTTIME).Append(System.DateTime.Now.ToString("HH:mm:ss"));
                url.Append(_MESSAGE).Append(UrlEncode(logs.Dequeue()));

                Debug.Log("LOG SENDING: " + url.ToString());

                UnityWebRequest www = UnityWebRequest.Get(url.ToString());
                yield return www.SendWebRequest();

                www.Dispose();
            }

            yield return null;
        }
    }

    string UrlEncode(string instring)
    {
        StringReader strRdr = new StringReader(instring);
        StringWriter strWtr = new StringWriter();
        int charValue = strRdr.Read();
        while (charValue != -1)
        {
            if (((charValue >= 48) && (charValue <= 57)) || ((charValue >= 65) && (charValue <= 90)) || ((charValue >= 97) && (charValue <= 122)))
                strWtr.Write((char)charValue);
            else if (charValue == 32)
                strWtr.Write("+");
            else
                strWtr.Write("%{0:x2}", charValue);

            charValue = strRdr.Read();
        }
        return strWtr.ToString();
    }
}
