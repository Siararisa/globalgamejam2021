﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;

[System.Serializable]
public class Charity
{
    public string charityName;
    public string charityLink;
    public Sprite charityImage;

    public Charity(Dictionary<string,object> dEntry)
    {
        charityName = Utilities.GetDictionaryKeyValue("charityName", dEntry);
        charityLink = Utilities.GetDictionaryKeyValue("charityLink", dEntry);
        charityImage = Resources.Load<Sprite>("Sprites/Charities/" + charityName) as Sprite;
    }
}
[System.Serializable]
public class Charities
{
    public List<Charity> charities;

    public Charities(Dictionary<string,object> dEntry)
    {
        charities = new List<Charity>();
        var dataList = dEntry["charities"] as List<object>;
        foreach (object obj in dataList)
        {
            var dataItem = obj as Dictionary<string, object>;
            charities.Add(new Charity(dataItem));
        }
    }
}
