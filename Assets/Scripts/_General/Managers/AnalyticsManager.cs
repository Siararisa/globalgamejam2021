﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using System;

public class AnalyticsManager : Singleton<AnalyticsManager> {

    protected override void Awake()
    {
        base.Awake();
        OnGameInit();
    }

    public void OnGameInit()
    {
        Debug.Log("initialized event");
        //DebugManager.Instance.LogMessage("ANALYTICS: gameInitialized");
        //RemoteAnalytics.Instance.AddEvent("Initialize Application");
        Analytics.CustomEvent("gameInitialized", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
        
    }

    public void OnGameQuit()
    {
        Debug.Log("quit event");
       // DebugManager.Instance.LogMessage("ANALYTICS: gameQuit");
        //RemoteAnalytics.Instance.AddEvent("Quit Application");
        Analytics.CustomEvent("gameQuit", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
       
    }


    
    public void OnNewLeader()
    {
        Debug.Log("leaderboard event");
       // DebugManager.Instance.LogMessage("ANALYTICS: leaderboard");
       // RemoteAnalytics.Instance.AddEvent("New Leader");
        Analytics.CustomEvent("leaderBoard", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

   
    
    public void OnMemoryGame()
    {
        Debug.Log("memoryGame event");
       // DebugManager.Instance.LogMessage("ANALYTICS: memoryGame");
        //RemoteAnalytics.Instance.AddEvent("Memory Game");
        Analytics.CustomEvent("memoryGame", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

    public void OnMemoryGameWin()
    {
        Debug.Log("memoryGameWin event");
        //DebugManager.Instance.LogMessage("ANALYTICS: memoryGameWin");
        //RemoteAnalytics.Instance.AddEvent("Memory Game Win");
        Analytics.CustomEvent("memoryGameWin", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

    public void OnMemoryGameLose()
    {
        Debug.Log("memoryGame event");
        //DebugManager.Instance.LogMessage("ANALYTICS: memoryGameLose");
        //RemoteAnalytics.Instance.AddEvent("Memory Game Lose");
        Analytics.CustomEvent("memoryGameLose", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

    public void OnReward()
    {
        Debug.Log("reward gift event");
       //DebugManager.Instance.LogMessage("ANALYTICS: rewardGift");
       // RemoteAnalytics.Instance.AddEvent("Gift Reward");
        Analytics.CustomEvent("rewardGift", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

    public void OnRewardWinner()
    {
        Debug.Log("reward winner event");
        //DebugManager.Instance.LogMessage("ANALYTICS: rewardWinner");
       // RemoteAnalytics.Instance.AddEvent("Reward Winner");
        Analytics.CustomEvent("rewardWinner", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

    public void OnNoReward()
    {
        Debug.Log("no reward event");
       // DebugManager.Instance.LogMessage("ANALYTICS: noReward");
       // RemoteAnalytics.Instance.AddEvent("No Reward");
        Analytics.CustomEvent("noReward", new Dictionary<string, object>
        {
            {"time", System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt")}
        });
    }

    
    protected override void OnApplicationQuit()
    {
        base.OnApplicationQuit();
        OnGameQuit();
    }

}
