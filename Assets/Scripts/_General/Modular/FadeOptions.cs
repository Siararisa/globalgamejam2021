﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FadeOptions: IFadeOptions 
{
	[SerializeField] FadeType _fadeType = FadeType.None;
	[SerializeField] bool _isFixedSpeed;//if enabled, uses fadeSpeed else uses fadeDuration
	[SerializeField] bool _isFadeFromStart;//if enabled, fade starts automatically else wait for fadeStart before fading; DOES NOT Apply to Fade in since it starts at the beginning
	[Range(0f, 1f)]
	[SerializeField] float _fadeSpeed;//amount to fade if using fixed speed
	[SerializeField] float _fadeInDuration;
	[SerializeField] float _fadeOutStart;
	[SerializeField] float _fadeOutDuration;

	public FadeType fadeType{get{return _fadeType;} set{_fadeType = value;}}
	public float fadeSpeed{get{return _fadeSpeed;} set{_fadeSpeed = value;}}
	public bool isFixedSpeed{get{return _isFixedSpeed;}set{_isFixedSpeed = value;}}
	public bool isFadeFromStart{get{return _isFadeFromStart;}set{_isFadeFromStart = value;}}
	public float fadeOutStart{get{return _fadeOutStart;} set{_fadeOutStart = value;}}
	public float fadeOutDuration{get{return _fadeOutDuration;} set{_fadeOutDuration = value;}}
	public float fadeInDuration{get{return _fadeInDuration;} set{_fadeInDuration = value;}}
}
