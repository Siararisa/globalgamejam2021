﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PooledObjectItem : MonoBehaviour
{
    [SerializeField] string id;

    public string GetID()
    {
        return id;
    }

    public void SetID(string value)
    {
        id = value;
    }
}

[System.Serializable]
public class ObjectPoolItem
{
    public string id;
    public GameObject objectToPool;
    public Transform parent;
    public int amountToPool;
    public bool shouldExpand;
}
