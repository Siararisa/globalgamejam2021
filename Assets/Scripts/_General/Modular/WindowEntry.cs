﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class WindowEntry : IWindowEntry {
	[SerializeField]private string _windowName;
	public string windowName{ get{ return _windowName; } set{ _windowName = value; } }
}
