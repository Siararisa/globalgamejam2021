﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugDataUI : MonoBehaviour {

    public Image icon;
    public Text message;
    public MessageType type;

    public void SetDebugData(DebugData data)
    {
        switch (data.type)
        {
            case MessageType.message:
                this.icon.sprite = Utilities.GetSpriteFromResources("Sprites/", "Debug/log");
                break;
            case MessageType.error:
                this.icon.sprite = Utilities.GetSpriteFromResources("Sprites/", "Debug/error");
                break;
            case MessageType.warning:
                this.icon.sprite = Utilities.GetSpriteFromResources("Sprites/", "Debug/warning");
                break;
        }
        this.message.text = data.message;
        this.type = data.type;
    }
}
