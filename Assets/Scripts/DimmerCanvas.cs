﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

using UnityEngine.Events;

public class DimmerCanvas : MonoBehaviour {

    public bool isFadeIn;
    public float fadeDuration = 1.0f;
    public Image dimmerImage;

    public UnityEvent OnFadeIn, OnFadeOut;

    
    private void OnEnable()
    {
        if (isFadeIn)
        {
            dimmerImage.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
            dimmerImage.DOColor(new Color(0.0f, 0.0f, 0.0f, 1.0f), fadeDuration).OnComplete(() => OnFadeIn.Invoke());
        } 
        else
        {
            dimmerImage.color = new Color(0.0f, 0.0f, 0.0f, 1.0f);
            dimmerImage.DOColor(new Color(0.0f, 0.0f, 0.0f, 0.0f), fadeDuration).OnComplete(() => OnFadeOut.Invoke());
        } 
    }


  
    public void FadeInDimmer(System.Action OnComplete = null)
    {
        dimmerImage.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        dimmerImage.DOColor(new Color(0.0f, 0.0f, 0.0f, 1.0f), fadeDuration).OnComplete(()=>OnComplete.Invoke());
    }

    public void FadeOutDimmer(System.Action OnComplete = null)
    {
        dimmerImage.color = new Color(0.0f, 0.0f, 0.0f, 1.0f);
        dimmerImage.DOColor(new Color(0.0f, 0.0f, 0.0f, 0.0f), fadeDuration).OnComplete(() => OnComplete.Invoke());
    }
}

