﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using MoreMountains.CorgiEngine;
public class TitleScreenCinematics : MonoBehaviour
{
    public Animator player, title;

    private PlayableDirector director;
    private bool isReady;

    // Start is called before the first frame update
    void Start()
    {
        director = GetComponent<PlayableDirector>();
        AudioManager.Instance.PlayTrack("Title");
    }

    float currentTime;
    float previousTime;
    // Update is called once per frame
    void Update()
    {
        previousTime = currentTime;
        if (Input.anyKeyDown && !isReady)
        {
            StartCutscene();
            isReady = true;
        }
    }

    void StartCutscene()
    {
        //director.Play();
        title.SetTrigger("loading");
        StartCoroutine(CheckAudio(()=> {
            StartCoroutine(FinishLoading());
        }));
    }

    IEnumerator FinishLoading()
    {
        title.SetTrigger("out");
        yield return new WaitForSeconds(4.0f);
        director.Play();
        StartCoroutine(InitiateCutscene());
    }

    IEnumerator InitiateCutscene()
    {
        yield return new WaitForEndOfFrame();
        previousTime = 0;
        StartCoroutine(CheckAudio(() =>
        {
            SwitchTracks();
        }));
    }

    IEnumerator CheckAudio(System.Action OnComplete)
    {
        while (AudioManager.Instance.IsTrackPlaying("Title"))
        {
            AudioTrackEntryController audio = AudioManager.Instance.GetTrack("Title");
            currentTime = audio.audioSource.time;
            if (currentTime < previousTime)
            {
                Debug.Log("Audio has looped");
                OnComplete.Invoke();
                yield break;
            }
            yield return null;
        }
    }

    public void Alert()
    {
        player.SetTrigger("Stand");
    }

    public void SwitchTracks()
    {
        AudioManager.Instance.FadeTrackToStop("Title");
        AudioManager.Instance.PlayTrack("Game");
    }
}
