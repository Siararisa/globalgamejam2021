﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class BellAcquisition : MonoBehaviour
{
    public CorgiController controller;
    public Animator animator;
    public GameObject bell;

    bool isAcquired;
    // Start is called before the first frame update
    void Start()
    {

    }

   
    public void GetBell()
    {
        if (controller.State.IsGrounded && Input.GetMouseButtonDown(0) && !isAcquired)
        {
            animator.SetTrigger("GetBell");
            MoreMountains.CorgiEngine.InputManager.Instance.InputDetectionActive = false;
            controller.gameObject.GetComponent<CharacterBell>().AcquireBell();
            isAcquired = true;
            bell.gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            GetBell();
        }
    }
}
